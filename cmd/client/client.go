package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi9/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi9Client struct {
	pb.Gogi9Client
}

func NewGogi9Client(address string) *Gogi9Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi9 service", l.Error(err))
	}

	c := pb.NewGogi9Client(conn)

	return &Gogi9Client{c}
}
